<?php

namespace App\Enums;

class ServicesEnum extends Enum
{
    public const AUTH_SERVICE_API = 'http://127.0.0.1:100';
    public const AUTH_SERVICE_VALIDATE_AUTHENTICATION_API = 'http://127.0.0.1:100/api/validate-authentication';
}
