<?php

namespace App\Http\Controllers;

use App\Http\Requests\BlogRequest;
use App\Models\Blog;
use Exception;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        try {
            $blogs = Blog::orderBy('id')->get();
            return successResponse('Blog List', $blogs, 200);
        } catch (Exception $ex) {
            return errorResponse($ex->getMessage(), null, $ex->getCode());
        }
    }


    /**
     * Store a newly created resource in storage.
     */
    public function store(BlogRequest $request)
    {
        try {
            Blog::withSlug('title', 'slug')->create($request->validate());
        } catch (Exception $ex) {
            return errorResponse($ex->getMessage(), null, $ex->getCode());
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        try {
            $blog = Blog::find($id);
            abort_unless($blog, 404, 'Blog Not Found');
            return successResponse('Blog', $blog, 200);
        } catch (Exception $ex) {
            return errorResponse($ex->getMessage(), null, $ex->getCode());
        }
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(BlogRequest $request, string $id)
    {
        try {
            $blog = Blog::find($id);
            abort_unless($blog, 404, 'Blog Not Found');
            $blog->update($request->validate());
        } catch (Exception $ex) {
            return errorResponse($ex->getMessage(), null, $ex->getCode());
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        try {
            $blog = Blog::find($id);
            abort_unless($blog, 404, 'Blog Not Found');
            $blog->delete();
        } catch (Exception $ex) {
            return errorResponse($ex->getMessage(), null, $ex->getCode());
        }
    }
}
