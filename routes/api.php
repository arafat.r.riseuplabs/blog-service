<?php

use App\Http\Controllers\BlogController;
use App\Http\Middleware\EnsureTokenIsValid;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware([EnsureTokenIsValid::class])->group(function () {
    Route::get('blogs', [BlogController::class, 'index']);
    Route::post('blogs/store', [BlogController::class, 'store']);
    Route::get('blogs/show/{id}', [BlogController::class, 'show']);
    Route::post('blogs/update/{id}', [BlogController::class, 'update']);
    Route::delete('blogs/delete/{id}', [BlogController::class, 'delete']);
});
